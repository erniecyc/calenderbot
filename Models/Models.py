# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

use_cuda = torch.cuda.is_available()


class Discriminator(nn.Module):
    r""" Discriminator in GAN 
    
    State Transition Estimator with 
            1) Multi-layer perceptron

    Args:
        dim (int): number of features.

    Returns:
        score : a prob score

    """
    def __init__(self, num_states):
        super(Discriminator, self).__init__() 

        self.num_states = num_states

        # Define dimensions
        init_feat = 200
        mlp_1 = init_feat
        mlp_2 = int(mlp_1/2)
        scalar_dim = 1

        # MLP
        self.mlp =  nn.Sequential(
                      nn.Linear(num_states,mlp_1),
                      nn.ReLU(),
                      nn.Linear(mlp_1,mlp_2),
                      nn.ReLU(),
                      nn.Linear(mlp_2,scalar_dim),
                      nn.Sigmoid()
                    )

    def forward(self,idx):

        one_hot_st = Variable(torch.zeros(1,self.num_states))
        one_hot_st[:,idx] = 1
        score = self.mlp(one_hot_st)
        return score






