# -*- coding: utf-8 -*-

import random

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim


use_cuda = torch.cuda.is_available()

def updateD(x_label,x_output,z_label,z_output,adv_criterion,D,lr=0.001):
    # Update discriminator, k = 1
    x_loss = adv_criterion(x_label,x_output,'real')
    z_loss = adv_criterion(z_label,z_output,'fake')
    for param in D.parameters():
        param.data -= lr * (x_loss.data+z_loss.data)
    return x_loss+z_loss

def updateG(z_label,z_output,adv_criterion,G,lr=0.001):
    # Update generator
    loss = adv_criterion(z_label,z_output,'real')
    for param in G.parameters():
        param.data -= lr * loss.data
    return loss