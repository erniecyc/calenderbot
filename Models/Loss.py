# -*- coding: utf-8 -*-


import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

class adv_criterion(nn.Module):

    def __init__(self, D):
        super(adv_criterion, self).__init__()
        self.D = D

    def forward(self,label,output,type_):
        if type_ in 'real':
            label_ = torch.ones(self.D(label).data.size())
            output_ = self.D(output)
            return F.binary_cross_entropy(self.D(label),self.D(output))
        else:
            label_ = torch.zeros(self.D(label).data.size())
            output_ = self.D(output)
            return F.binary_cross_entropy(self.D(label),self.D(output)) 

    def backward(self,grad_output): 
        return grad_output, None
    

    







