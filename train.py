# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, division
import random
import argparse

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim
import numpy as np

import Models.Models as MM
import Models.Adversarial as adv
from Bot import calendarBot as c
import Models.Loss as l


use_cuda = torch.cuda.is_available()

LR = 0.01
NUM_STATES = 6

# Data and loading options
parser = argparse.ArgumentParser(description='Options to determine ways to test bot.')
parser.add_argument('-user', action='store_true',
                             help='Mode of testing.',
                             default=False)
parser.add_argument('-token', required=False,
                             help='Set access token.',
                             default='b74e4fd005464d24b38b7812ca0a9ee4')
parser.add_argument('-threshold', required=False,
                             help='Number of epochs to start saving.',
                             type=int,
                             default=0.95)
parser.add_argument('-loss', required=False,
                             help='Specify loss compuation.',
                             type=str,
                             default='cross')
parser.add_argument('-mag', required=False,
                             help='Specify magnitude of duplicate.',
                             type=int,
                             default=500)
options = parser.parse_args()

def main():

    # Generator
    G = c.calendarBot(options.token,is_train=True)
    curr_state = G.general_state
    bot_response = curr_state('hi Chris!') # pre-condition is single intiative

    # Prepare data
    mag = options.mag
    xlabels = [1,3,4,5]*mag
    user_responses = ['10:45','10/10/2017','Paris','work']*mag

    THRESHOLD = int(mag-5)


    if options.loss in 'gan':

        # Prepare fake data
        fake_user_responses = ['10:45','10/10/2017','Paris','work']*mag
        random.shuffle(fake_user_responses)

        # Init with start state
        next_state_x = curr_state
        next_state_z = curr_state

        # Discriminator
        D = MM.Discriminator(NUM_STATES)
        adv_criterion = l.adv_criterion(D)

        # Init optimizers
        D_optimizer = optim.Adam(D.parameters(), lr=LR)
        G_optimizer = optim.Adam(G.parameters(), lr=LR)
        D_optimizer.zero_grad()
        G_optimizer.zero_grad()

        # Train
        epoch = 0
        for user_response,fake_user_response,xlabel in \
                        zip(user_responses,fake_user_responses,xlabels):

            # Generate data
            next_dict = G(next_state_x,user_response) # real
            next_state_x = next_dict['next_state'] 
            bot_response = next_dict['bot_response'] 
            next_idx_x = next_dict['next_idx'] 

            next_dict = G(next_state_z,fake_user_response) # fake
            next_state_z = next_dict['next_state'] 
            fake_bot_response = next_dict['bot_response'] 
            next_idx_z = next_dict['next_idx'] 

            # Show bot response
            print(bot_response)

            # Generate fake Z label
            z_label = random.randint(0,NUM_STATES-1)

            # Update D 
            d_loss = adv.updateD(xlabel,next_idx_x,
                                 z_label,next_idx_z,
                                 adv_criterion,D,lr=LR)

            # Step D optimizer
            D_optimizer.step() 

            # Update G 
            g_loss = adv.updateG(z_label,next_idx_z,
                                 adv_criterion,G,lr=LR)

            # Step G optimizer
            G_optimizer.step()

            # Calculate and show loss
            loss = (d_loss.data[0],g_loss.data[0])
            print(loss)

            # Save Model 
            if epoch>THRESHOLD:
                checkpoint = {
                    'D': D,
                    'G': G,
                    'D_optimizer': D_optimizer,
                    'G_optimizer': G_optimizer
                }
                torch.save(checkpoint,open('save_models/model_'+str(epoch),'wb'))

            epoch += 1 

    elif options.loss in 'cross':

        # teacher-forcing
        epoch = 0

        criterion = nn.CrossEntropyLoss()
        G_optimizer = optim.Adam(G.parameters(), lr=LR)

        for user_response,next_idx_label in zip(user_responses,xlabels):

            # Advance
            next_dict = G(curr_state,user_response)

            bot_response = next_dict['bot_response']
            output = next_dict['next_idx_vec']
            curr_state = next_dict['next_state']
            label = Variable(torch.LongTensor(np.array([next_idx_label])))

            # Calculate loss
            loss = criterion(output,label)

            # Backprop
            loss.backward()
            
            # Change LR
            G_optimizer.step()

            # Show loss
            print('CrossEntropyLoss: ',loss.data[0])

            # Save Model 
            if epoch>THRESHOLD:
                checkpoint = {
                    'G': G,
                    'G_optimizer': G_optimizer
                }
                torch.save(checkpoint,open('save_models/model_'+str(epoch),'wb'))

            epoch += 1 


if __name__ == "__main__":
    import time
    start = time.time() 
    main()
    print("Time taken {}".format(time.time()-start))




