# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import numpy as np

"""
State classes manages dialog responses.

Args:
    user_response

Returns:
    bot_response

"""

class stateBase(nn.Module):
    r"""
    stateBase class as a generic framework for all states.
    
    """
    def __init__(self,**args):
        super(stateBase, self).__init__()
        self.name = args['stateName']
        self.responses = args['responses']
        self.entity_names = args['entity_names']

    def is_vulgar(self,user_response):
        """ Check if user response contains vulgarities """
        from functools import reduce 
        vulgarities = ['fuck','bitch'] # could use vulgar dictionary
        return reduce((lambda x, y: x in user_response \
                    or y in user_response), vulgarities)

    def forward(self,user_response):
        r"""
        Args:
            user_response (string): string given by user.
        Returns:
            bot_response (string): string for bot_response.
        """
        raise NotImplementedError 

class promptState(stateBase):
    r"""
    promptState class is a generic framework for states that prompt user input.
    
    """
    def __init__(self,**args):
        super(promptState, self).__init__(**args)
        self.entities = {}
        self.init()

    def init(self):
        """ Init slots and responses """
        for entity in self.entity_names:
            self.entities[entity] = []

        # Paraphrase response 
        exp_responses = []     
        for response in self.responses:  
            exp_responses += self.paraphrase(response)
        self.responses = exp_responses

    def addEntities(self,key,value):
        """ Return true if set. """
        if key in self.entities.keys():
            self.entities[key] = value
            return True
        else:
            return False

    def paraphrase(self,sent):
        """ Paraphrasing tool """
        import subprocess
        command = "echo \""+sent+"\" | \
                  ./paraphrase_tool/ace -g paraphrase_tool/erg-1212-osx-0.9.26.dat \
                  -1T 2>/dev/null | ./paraphrase_tool/ace \
                  -g paraphrase_tool/erg-1212-osx-0.9.26.dat -e"
        output = subprocess.check_output(command,shell=True)
        paraphrases = output.decode('utf-8').split('\n')[:-2]
        import random 
        if len(paraphrases)>0:
            return paraphrases
        else:
            return [sent]

    def forward(self,user_response):
        """ Generate response """
        bot_response = 'Please watch your language!\n' \
                            if self.is_vulgar(user_response) else ''
        import random
        bot_response = bot_response+'\n'+random.choice(self.responses)
        print(bot_response)
        return bot_response















