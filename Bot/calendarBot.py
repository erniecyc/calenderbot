# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import Bot.state as state
import nlu as n
import Models.Models as model



class calendarBot(nn.Module):
    r"""
    calendarBot is a class that interacts with users.
    It contains 6 default states in total.
    
    """
    def __init__(self,access_token,is_train):
        super(calendarBot, self).__init__()

        self.is_train = is_train

        # Init calendar bot bodies
        self.num_states = 6
        self.END = self.num_states-1
        self.nlu = n.nlu(access_token)
        self.states_tracker = torch.ones(self.num_states,self.num_states)
        self.s2idx = {}

        # slot names 
        self.date_ = 'date'
        self.time_ = 'time'
        self.location_ = 'location'
        self.event_ = 'event'

        # list of all states
        self.initStates()
        self.states = [ self.general_state, self.date_state, 
                        self.time_state, self.location_state,
                        self.event_state, self.end_state ]

        # Init name to index
        for i,state in zip(range(self.num_states),self.states):
            self.s2idx[state.name] = i

        # Start and end states are to have zero probs
        idp_names = ['general','end']
        self.idp_states(idp_names)
        self.idp_states_idx = [ self.s2idx[name] for name in idp_names ]

        # list of past states
        self.state_mem = [self.general_state]

        # Transition matrix M
        self.M = Variable(torch.randn(self.num_states, self.num_states),requires_grad=True)

        # Encode each state's one-hot
        self.stateEncoder = nn.Linear(self.num_states, self.num_states)
        self.nonlinear = nn.Sigmoid()

        self.embed_shift = nn.Linear(self.num_states*2, self.num_states)

        # State sequence representation
        self.gru = nn.GRU(self.num_states, self.num_states, num_layers=1)

    def idp_states(self,names):
        for name in names:
            self.states_tracker[:,self.s2idx[name]] = 0

    def initStates(self):
        """ Define Bot body """

        # Default states for tracking
        defaultStates = torch.ones(1,self.num_states)

        # Init bot states, could also read from JSON 
        print('Initializing...')
        self.general_state = state.promptState(stateName='general',
                                               entity_names=['greet'],
                                               responses=[ 'Please tell me the date, time, \
                                            location and the event that you want to schedule.', \
                                            'How are you doing ? Would you like to schedule?' ])
        self.date_state = state.promptState(stateName=self.date_,
                                            entity_names=['date'],
                                            responses=['Please give me the date.'])
        self.time_state = state.promptState(stateName=self.time_,
                                            entity_names=['time' , 'time-period'],
                                            responses=['Please give me the time.'])
        self.location_state = state.promptState(stateName=self.location_,
                                                entity_names=['address', 'sysloc', 'capitals', 'uscity', 'geo-city', 'Location'],
                                                responses=['Please give me the location.'])
        self.event_state = state.promptState(stateName=self.event_,
                                             entity_names=['event'],
                                             responses=['Give me the name of the event or activities, please.'])
        self.end_state = state.promptState(stateName='end',
                                           entity_names=['end'],
                                           responses=['Thank you! Have a nice day!'])
        print('Done.\n\n')

    def resetStates(self):
        for i in range(self.num_states):
            self.states_tracker[:,i] = 1
        # Start and end states are to have zero probs
        self.states_tracker[:,self.s2idx['general']] = 0
        self.states_tracker[:,self.s2idx['end']] = 0

    def showAllEntities(self):
        import json
        print('\n\n+++++++++++Sample Json Dump+++++++++++')
        for state in self.states:
            if len(state.entities.keys())>0:
                event = {}
                event['intent'] = state.name
                event['entities'] = state.entities 
                print(json.dumps(event))

    def probTransition(self,states_set):
        """ Probabilistic state transition function """

        # Build S : [1, num_state]
        states_cat = []
        for state in self.state_mem[:self.num_states]:
            one_hot_st = Variable(torch.zeros(1,self.num_states))
            st = self.s2idx[state.name]
            one_hot_st[:,st] = 1
            states_cat.append(self.stateEncoder(one_hot_st))
        
        rnn_input = torch.cat(states_cat,dim=0).unsqueeze(0).transpose(0,1) # [num_state, num_state]
        outputs, S = self.gru(rnn_input)

        # Build E : [1, num_state]
        E = Variable(torch.zeros(1,self.num_states))
        for idx in states_set:
            one_hot_st = Variable(torch.zeros(1,self.num_states))
            one_hot_st[:,idx] = 1
            E += self.stateEncoder(one_hot_st)

        # Building C = [S:E] : [ 1, 2 x num_state ]
        C = torch.cat([S.view(1,-1),E],dim=1)
        C_ = self.embed_shift(C)

        # max( logSoftmax( M x C ) )
        next_idx_vec = F.log_softmax( torch.mm(C_,self.M) )

        max_idx = torch.max( next_idx_vec, 1 )
        next_idx = max_idx[1][0].data.numpy()[0]

        # Get next state
        next_state = self.states[next_idx]

        next_dict = { 'next_state': next_state,
                      'next_idx': next_idx, 
                      'next_idx_vec': next_idx_vec }

        return next_dict

    def transition(self,states_set):
        """ State transition function """

        # Go to independent states
        if any( x in self.idp_states_idx for x in states_set ):
            # Get next state
            next_idx = states_set[0] # opt for first index
            next_state = self.states[next_idx]
            next_dict = { 'next_state': next_state, 
                          'next_idx': next_idx }
            return next_dict

        # Set indices of tracker
        for i in states_set:
            self.states_tracker[:,i] = 0

        # Resolve next state index
        max_idx = torch.max(self.states_tracker,1)
        next_idx = int(max_idx[1].numpy()[0])

        # Determine if end state
        all_same = lambda items: len(set(items))==1 and items[0]<1
        if all_same(self.states_tracker[:,1:-1].numpy()[0]): 
            next_idx = self.END
            self.resetStates()

        # Get next state
        next_state = self.states[next_idx]

        next_dict = { 'next_state': next_state, 
                      'next_idx': next_idx }

        return next_dict

    def forward(self,curr_state,user_response):
        r"""
        Determine which slots are missing from user_response
        based on the new info, decide which states to go next
        by computation from max(curr_state.nextState)

        Args:
            curr_state (stateBase): stateBase object.
            user_response (string): string given by user.
        Returns:
            next_state (stateBase): stateBase object.
            bot_response (string): reply to humans,
        """

        import random
        states_set = self.nlu.getEntities(session_id=str(random.randint(1,1000)),
                                          query=user_response, 
                                          states=self.states,
                                          states_tracker=self.states_tracker)
        if self.is_train:
            next_dict = self.probTransition(states_set)
        else:
            next_dict = self.transition(states_set)

        # Store bot response
        next_state = next_dict['next_state']
        bot_response = next_state(user_response)
        next_dict['bot_response'] = bot_response

        # Store state into memory
        self.state_mem.append(curr_state)

        return next_dict












