# calenderBot

The task is to create a simple bot that allows a user to add an appointment to his calendar using natural language(text-based, not speech-based). 

To-Do:

- [x] Set-up of calendarBot
- [x] Push trained model for probabilistic state transition
- [x] SEQ2SEQ User simulator



Each calendar event is assumed to have:

- 1) Date
- 2) Time
- 3) Location
- 4) Event

Requirements on Python 3.5.4:

```

pip install -r requirements.txt

apiai==1.2.3
numpy==1.13.1
torch==0.2.0.post4

```


To run, simply type:

```
usage: test.py [-h] [-u] [-l] [-token TOKEN] [-checkpoint CHECKPOINT]

Options to determine ways to test bot.

optional arguments:
  -h, --help            show this help message and exit
  -u                    Specify mode of testing to be user interactive.
  -l                    Specify whether to load pre-trained model.
  -token TOKEN          Set access token.
  -checkpoint CHECKPOINT
                        Specify checkpoint file path.

```

For user-interactive mode:

```
python test.py -u

```

