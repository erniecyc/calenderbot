# -*- coding: utf-8 -*-

import argparse

import torch

from Bot import calendarBot as c

# Data and loading options
parser = argparse.ArgumentParser(description='Options to determine ways to test bot.')
parser.add_argument('-u', action='store_true',
                             help='Specify mode of testing to be user interactive.',
                             default=False)
parser.add_argument('-l', action='store_true',
                             help='Specify whether to load pre-trained model.',
                             default=False)
parser.add_argument('-token', required=False,
                             help='Set access token.',
                             default='b74e4fd005464d24b38b7812ca0a9ee4')
parser.add_argument('-checkpoint', required=False,
                             help='Specify checkpoint file path.',
                             default='save_models/model_999')
options = parser.parse_args()

def main():

    if options.l:
        checkpoint = torch.load(options.checkpoint)
        bot = checkpoint['G']
    else:
        bot = c.calendarBot(options.token,is_train=False)

    curr_state = bot.general_state
    bot_response = curr_state('hi Chris!') # pre-condition is single intiative

    if options.u:
        userInputMode(bot,bot_response,curr_state)
    else:
        simulate(bot,curr_state)

    bot.showAllEntities()

def simulate(bot,curr_state):
    # simulator testing mode
    import time
    import random
    
    user_response_A = ['10:45','10/10/2017','Paris','work']
    random.shuffle(user_response_A)
    user_response_B = ['10:45','10/10/2017','Paris','work']
    random.shuffle(user_response_B)

    user_responses = user_response_A + user_response_B

    for user_response in user_responses:
        time.sleep(random.randint(1, 3))
        print(user_response)
        next_dict = bot(curr_state,user_response)
        curr_state = next_dict['next_state']
        bot_response = next_dict['bot_response']

def userInputMode(bot,bot_response,curr_state):
    # User response testing mode
    while True:
        user_response = input()
        next_dict = bot(curr_state,user_response)
        curr_state = next_dict['next_state']
        bot_response = next_dict['bot_response']

if __name__ == "__main__":
    import time
    start = time.time() 
    main()
    print("\n\nTime taken {}".format(time.time()-start))










