# -*- coding: utf-8 -*-

import apiai as api
import json

class nlu:
    r"""
    nlu is a class that process user input, extract entities.
    
    """
    def __init__(self,access_token):
        super(nlu, self).__init__()
        self.CLIENT_ACCESS_TOKEN = access_token

    def jsonParser(self,response):
        """ Json parser """

        response = response.decode('utf8')
        fields = json.loads(response) 
        return fields

    def request(self,session_id,query):
        """ Make api request to api.ai """

        ai = api.ApiAI(self.CLIENT_ACCESS_TOKEN)
        request = ai.text_request()
        request.session_id = session_id # <SESSION ID, UNIQUE FOR EACH USER>
        request.query = query
        response = request.getresponse().read()
        return self.jsonParser( response )

    def getEntities(self,session_id,query,states,states_tracker):
        """ Obtain entities from apiai """

        fields = self.request(session_id,query)
        states_set = [] # store indices of states set

        # Resolve empty response
        entity_num = len(fields['result']['contexts'])
        if entity_num==0:
            return states_set

        # Get entities
        entities = fields['result']['contexts'][-1]['parameters']

        # Store slot values
        for i in range(len(states)):
            for entity,value in entities.items():
                if str(value) in '': continue
                is_set = states[i].addEntities(entity,value)
                if is_set:
                    states_set.append(i)

        return states_set


















